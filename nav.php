<?php
require_once "bd.php";


try {
    $requeteSql = "SELECT * FROM categorie";
    // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
    $requetePreparee = $db->prepare($requeteSql);
    // On execute la requête préparée 
    $requetePreparee->execute();
    // On renvoi l'ensemble des résultats de la requête
    $categories = $requetePreparee->fetchAll();
} catch (Exception $exception) {
    echo $exception->getMessage();
}

?>

<!-- les liens principaux de navigation -->

<nav class="nav">
    
            <ul class="options">
                <li class="lien"><a href="gererMesTheme.php">Gérer Mes thémes</a></li>
                <li class="lien"><a href="categorie.php">Ajouter une Catégorie</a></li>
                <li class="lien"><a href="ajouterTheme.php">Ajouter Un Théme</a></li>
                <li class="lien"><a href="creeUnecarte.php">Crée Une Carte</a></li>
                <li class="lien"><a href="afficherCategorie.php">Afficher Vos Catégories</a></li>
                <li class="lien"><a href="afficherTheme.php">Afficher Vos Thémes</a></li>
                <li class="lien"><a href="revision.php">Créé Une Révision</a></li>
                <li class="lien"><a href="mesRevision.php">Mes Révisions</a></li>
                <li class="lien"><a href="deconnecter.php">Se deconnecter</a></li>
            </ul>

       
    </div>
</nav>