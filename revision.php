<?php
// on récupère le fichier session.php et bd.php 
require_once 'session.php';
require_once 'bd.php';
include 'header.html';
// require 'functions.php';



session_start();
//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
    header('Location:connexion.php');
}
echo $_SESSION['id_categorie'];
//a continuer

if (isset($_POST['submit'])) {
    if (!empty($_POST['theme']) && !empty($_POST['nbrDeCarte']) && !empty($_POST['nbrDeNiveau'])) {

        $theme = $_POST['theme'];
        $theme = htmlspecialchars($theme);

        $nb_carte = $_POST['nbrDeCarte'];
        $nb_carte = htmlspecialchars($nb_carte);

        $nb_niveau = $_POST['nbrDeNiveau'];
        $nb_niveau = htmlspecialchars($nb_niveau);

        $started_at = $_POST['started_at'];
        // $started_at = htmlspecialchars($started_at);
        $started_at = strtotime($started_at);
        $started_at = date('Y-m-d', $started_at);
        // echo $started_at;echo '<br>';


        try {

            $requetSql = "INSERT INTO revision (nb_niveau,nb_carte,id_utilisateur,id_theme,started_at) VALUES (:nb_niveau,:nb_carte,:id_utilisateur,:id_theme,:started_at) ";
            $requetSql = $db->prepare($requetSql);
            $requetSql->bindValue(':nb_niveau', $nb_niveau, PDO::PARAM_INT);
            $requetSql->bindValue(':nb_carte', $nb_carte, PDO::PARAM_INT);
            $requetSql->bindValue(':id_utilisateur', $_SESSION['idUser'], PDO::PARAM_INT);
            $requetSql->bindValue(':id_theme', $theme, PDO::PARAM_INT);
            $requetSql->bindValue(':started_at', $started_at, PDO::PARAM_STR);

            ////
            $requetSql->execute();
            $revision = $requetSql->fetch();
            header('Location:mesRevision.php?id_theme=' . $theme);
        } catch (PDOException $exception) {
            // echo $exception->getMessage();
            //recuperer l'erreur et le stocker dans un variable pour afficher aprés un message d'erreur 
            // var_dump($exception);
            if ($exception->errorInfo[1] == 1062) {

                $_SESSION["msg"] = " Vous avez déjà créé cette révision ";
            }
        }
    }
}
?>






<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script src="app.js" defer></script>
    <title>Révision</title>
</head>

<body>
    <!--form container-->
    <?php include "header.php"; ?>

    <div>
        
        <main class="contenaire">
            
            
            <div class="grid">
                <div class="wrapper">
                    <form action="" method="POST">
                        <h1>Révision</h1>
                        <div class="field-container">
                            <label for="theme">Theme:<span class="required">*</span></label>
                            <!-- <input type="text" name="theme" id="theme" placeholder= "" required /> -->
                            <?php include('themesOption.php'); ?>

                            <span class="error-messg">
                                <!-- tester si la session contient un message d'erreur, 
                                            l'afficher, 
                                    l'enlever de la session -->
                                <?php
                                if (isset($_SESSION["msg"])) {
                                    echo "<div class = 'required center' ><p>{$_SESSION["msg"]}</p></div>";
                                }
                                ?>
                            </span>
                        </div>
                        <div class="field-container">
                            <label for="nbr de Carte">nbr de Carte: <span class="required">*</span></label>
                            <input type="number" name="nbrDeCarte" id="" placeholder="" required />
                            <span class="error-messg"></span>
                        </div>
                        <div class="field-container">
                            <label for="nbr de Niveau">nbr de Niveau: <span class="required">*</span></label>
                            <input type="number" name="nbrDeNiveau" id="" placeholder="" required />
                            <span class="error-messg"></span>
                        </div>
                        <div class="field-container">
                            <label for="date">Date: <span class="required">*</span></label>
                            <input type="date" name="started_at" id="" placeholder="" required />
                            <span class="error-messg"></span>
                        </div>
                        <div class="center"><input type="submit" name="submit" value="Valider">
                        </div>
                    </form>
                </div>
            </div>
        </main>


    </div>

</body>

</html>