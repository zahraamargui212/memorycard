<?php
//demarer une session
session_start();

ini_set('display_errors', 1);
error_reporting(E_ALL);
// on récupère le fichier session.php
require_once 'session.php';
// je vérifie si la clé msg existe en session
if (isset($_SESSION["msg"])) {
    echo $_SESSION["msg"];
}

require_once 'bd.php';



?>



<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>memoryCard</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>




    <!--Main wrapper-->
    <div class="wrapper index">
        <h1>Connexion</h1>
        <!--   Pour revenir a la racine du site     ../    -->
        <p id="info">Bienvenue sur memoryCard, si vous n'êtes pas inscrit <a href="../"> inscrivez-vous</a>. </p>

        <!--form container-->
        <div class="form-container">
            <form novalidate action="" method="post">
                <!--flexbox and it's items-->
                <div class="flex">
                    <div class="flex-item">
                        <!--Pseudo field-->
                        <!-- <div class="field-container">
                            <label for="name">name: <span class="required">*</span></label>
                            <input type="text" name="name" pattern="^([a-zA-Z]{2,} ?)+$" id="name"
                                placeholder="Your pseudo" required="required" />
                            <span class="error-messg"></span>
                        </div> -->

                        <div class="field-container">
                            <label for="pseudo">Pseudo: <span class="required">*</span></label>
                            <input type="text" name="pseudo" id="pseudo" placeholder="Ex :zahra" required />
                            <span class="error-messg"></span>
                        </div>


                        <!--email field-->
                        <div class="field-container">
                            <label for="email">Email: <span class="required">*</span></label>
                            <input type="email" name="email" id="email" placeholder="Ex : zahra@gmail.com" required />
                            <span class="error-messg"></span>
                        </div>



                    </div>
                    <div class="flex-item">


                        <!--password field-->
                        <div class="field-container">
                            <label for="passkey">Mot de passe: <span class="required">*</span></label>
                            <div class="passkey-box">
                                <input type="password" minlength="5" name="passkey" id="passkey" class="passkey" placeholder="******" required="required" />
                                <p style="display:none" id="error">Non d'utilisateur ou mot de passe Icorrect !!</p>
                                <span class="passkey-icon" data-display-passkey="off"><i class="fas fa-eye"></i> </span>
                            </div>
                            <span class="error-messg"></span>
                        </div>

                    </div>
                </div>
                <!--Submit button & CGU-->

                <div class="center"><input type="submit" name="ok" value="Connexion"></div>

                <!--deconnexion-->


            </form>
        </div>




    </div>
    </form>
    </div>

    </div>
    <!-- <script src="script.js"></script>-->
    <?php
    if (isset($_POST['ok'])) {
        if (!empty($_POST['passkey'])  && !empty($_POST['email']) && !empty($_POST['pseudo'])) {

            $pseudo = $_POST['pseudo'];
            $pseudo = htmlspecialchars($_POST['pseudo']);

            $pass = $_POST['passkey'];
            // $hashed_pass = password_hash($pass, PASSWORD_DEFAULT);

            $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

            if ($email) {
                $email = htmlspecialchars(trim($email));
            }
            ////////correction///////////////////
            if (!$email) {
                echo 'votre email est invalide ';
            } else {

                /////////////////////correction///////////////////


                $sql = "SELECT * FROM utilisateur WHERE email=:email AND  pseudo = :pseudo";

                $reponse = $db->prepare($sql);
                $reponse->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
                //$reponse->bindValue(':password', $pass, PDO::PARAM_STR);
                $reponse->bindValue(':email', $email, PDO::PARAM_STR);
                $reponse->execute();
                $user = $reponse->fetch();

                if ($user) {
                    // On a un utilisateur, il faut verfier le mot de pass
                    /////////////////::correction::::::::::::::::::
                    $passUser = $user['password'];
                    if (password_verify($pass, $passUser)) {

                        $_SESSION['username'] = $user['pseudo'];
                        // recuprer un id du l'utilisateur conncter(table utilisateur)
                        $_SESSION['idUser'] = $user['id_utilisateur'];
                        echo  $_SESSION['idUser'];
                        header('Location:afficherCategorie.php');
                    } else {
                        echo "Votre Mot de passe est incorrect";
                    }
                } else {
                    echo "infos de connexion invalides ";
                }
                // print_r($reponse->rowCount()) ;
                // header('Location:deconnexion.php');
            }
        }
    }


    ?>

</body>

</html>