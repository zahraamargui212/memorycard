<?php
// on récupère le fichier session.php et bd.php 
require_once 'session.php';
require_once 'bd.php';
include 'header.html';

session_start();
//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
    header('Location:connexion.php');
}
/// on verfier l'id de utilisateur si c'est le même id connecter  
if (empty($_GET['id_theme'])) {
    header('Location: gererMesTheme.php');
}
$id = $_GET['id_theme'];

// print_r ($_POST);

if (isset($_POST['modifier']) && isset ($_POST['id_categorie'] )) {
    
    //récuprer id_cat
    $theme = htmlspecialchars(trim($_POST['nomTheme']));
    $public = 0;  
    if(isset($_POST['public']) && $_POST["public"] == true) {
        $public = 1;
    } 
    $id_cat =$_POST['id_categorie'];
    try {
        $requeteSql = "UPDATE theme SET public=:public,nom=:nomTheme, id_categorie=:id_cat WHERE  id_theme=:id; ";
        $requetePreparee = $db->prepare($requeteSql);
        // $requetePreparee->bindValue(':nomCategorie', $catgorie, PDO::PARAM_STR);
        $requetePreparee->bindValue(':id', $id, PDO::PARAM_INT);
        $requetePreparee->bindValue(':public', $public, PDO::PARAM_INT);
        $requetePreparee->bindValue(':nomTheme', $theme, PDO::PARAM_STR);
        // id_ct
        $requetePreparee->bindValue(':id_cat',$id_cat, PDO::PARAM_INT);
        $requetePreparee->execute();
        echo "Nombre de ligne modifiée " . $requetePreparee->rowCount();
        echo "bien Modifier";
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}



/**
 * Récupere theme de categorie
 * @param \PDO $db
 * @param  $id
 * @return Array  le theme a modifier 
 */
function theme($db, $id)
{
    $requeteSql = "SELECT theme.id_utilisateur,theme.id_categorie,theme.id_theme,theme.nom as nomTheme,public,
    categorie.nom as nomCategorie
    FROM theme 
    INNER JOIN categorie 
    on theme.id_categorie=categorie.id_categorie
    where id_theme = :idTheme"; // $id= $_GET['id_categorie'];
    $requetePreparee = $db->prepare($requeteSql);
    $requetePreparee->bindValue(":idTheme", $id);
    $requetePreparee->execute();
    return $requetePreparee->fetch();
}
$theme = theme($db, $id);
// echo '<pre>';
// print_r($theme);
// echo '</pre>';
// echo $theme['public'];

// foreach ($resultatThemes as $resultat) {
//     echo $resultat['nomTheme'];
// }


?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title> Modifier</title>
</head>

<body>
    <main class="contenaire">
        <?php include "nav.php"; ?>
        <div class="grid">

            <!--form container-->
            <div class="wrapper">
                <h1>Modifier</h1>
                <div class="form-container">
                    <form novalidate action="" method="post" enctype="">
                        <!--flexbox and it's items-->
                        <div class="flex">
                            <div class="flex-item">
                                <div class="field-container">
                                    <label for="name">Categorie : <span class="required">*</span></label>
                                    <div class="option">
                                        <!-- trouver comment récupérer la valeur de l'option selectionnée -->
                                        <select id="cat-select" name="id_categorie"> 
                                            <option value="">Les categorie</option>
                                            <?php foreach ($categories as  $categorie) : ?>
                                               <!-- si id_categorie de $categorie (categorie) == id_categorie de $theme (theme) on affiche categorie selected attribut HTML -->
                                                <option value="<?php echo $categorie["id_categorie"]; ?>"
                                                
                                                <?php if($categorie['id_categorie']==$theme['id_categorie']){ echo " selected";} ?>
                                                ><?php echo $categorie["nom"]; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        
                                    </div>

                                    <div class="field-container">
                                        <label for="name">Thème : <span class="required">*</span></label>
                                        <input type="text" name="nomTheme" id="name" value="<?php echo $theme['nomTheme']; ?>" placeholder="Ex: foot" required />

                                        <span class="error-messg"></span>
                                    </div>



                                    <div class="checkbox-wrapper-7">Public
                                        <input class="tgl tgl-ios" name="public" id="cb2-7" type="checkbox"   <?php if($theme['public']==1){ echo "checked";}?>/>
                                        <label class="tgl-btn" for="cb2-7">
                                    </div>
                                    <div class="center"><input type="submit" name="modifier" value="Valider"> </div>

                                </div>

                            </div>

                    </form>
                </div>
            </div>
        </div>
    </main>
</body>

</html>
<?php

?>