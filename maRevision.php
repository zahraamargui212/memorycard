<?php

// on récupère le fichier session.php et bd.php 

require_once 'session.php';
require_once 'bd.php';
require_once 'functions.php';


//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
    header('Location:connexion.php');
}


if (empty($_GET['id_revision'])) {
    header('Location:mesRevison.php');
    exit;
}
$idRevision = filter_var($_GET['id_revision'], FILTER_VALIDATE_INT);
if ($idRevision == false) {
    header('Location:mesRevison.php');
    exit;
}


/**
 * Récupère  la revision (id_revision)
 * @param int $idRevision
 * @param \PDO $db
 * @return array
 */
function get_revision(\PDO $db, $idRevision)
{
    try {
        $requetSql = "SELECT * FROM revision WHERE id_revision = :id";
        // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
        $requetePreparee = $db->prepare($requetSql);
        $requetePreparee->bindValue(':id', $idRevision, PDO::PARAM_INT);
        // On execute la requête préparée 
        $requetePreparee->execute();
        // On renvoi l'ensemble des résultats de la requête
        return $requetePreparee->fetch();
    } catch (Exception $exception) {
        echo $exception->getMessage();
    }
}
$get_revisions = get_revision($db, $idRevision);
// echo "<pre>";
// var_dump($get_revisions);
// echo "</pre>";
////////////////////



$started_at = $get_revisions['started_at'];
//$interval c'est la difference entre la date de création et la date du jour

$origin = new DateTimeImmutable($started_at);
$target = new DateTimeImmutable();
$interval = date_diff($origin, $target)->d;
// echo $interval->format('%R%a days');



$nbrN =  $get_revisions['nb_niveau'];
$nbrCarte = $get_revisions['nb_carte'];
$idThemeRevision = $get_revisions['id_theme'];

//en vérifier si on est pas  entrain de jouer

if (empty($_POST['continue']) && empty($_POST['reessaie'])) {

    //il faut récuperéé nb nouvelle carte du theme de la revision qui sont pas dans la révision 
    //insérer ces carte dans la table revoit au niveau 1
    try {

        $requetSql = "INSERT INTO revoit (id_carte,id_revision,niveau) 
                 SELECT id_carte,$idRevision,1 FROM carte 
                  WHERE id_theme = :id_theme 
                  AND id_carte NOT IN (SELECT id_carte FROM revoit WHERE id_revision = :id_revision)
                  ORDER BY RAND() LIMIT :nb_carte";
        $statement = $db->prepare($requetSql);
        $statement->bindParam("id_revision", $idRevision, PDO::PARAM_INT);
        $statement->bindParam("id_theme", $idThemeRevision, PDO::PARAM_INT);
        $statement->bindParam("nb_carte", $nbrCarte, PDO::PARAM_INT);
        $statement->execute();
    } catch (Exception $exception) {
        echo $exception->getMessage();
    }
}
$niveauZero = 2 ** 0;

if (isset($_POST['continue']) && !empty($_POST['idCarte']) && filter_var($_POST['idCarte'], FILTER_VALIDATE_INT)) {
    echo "coucou" . $_POST['idCarte'];
    try {
        $requeteSQL = "UPDATE  revoit SET niveau=niveau+1, dernier_vue =:dernier_vue WHERE id_carte=:id_carte";
        $requeteSQL = $db->prepare($requeteSQL);
        // $requeteSQL->bindValue(':niveau', $niveau, PDO::PARAM_INT);
        $requeteSQL->bindValue(':id_carte', $_POST['idCarte'], PDO::PARAM_INT);
        $requeteSQL->bindValue(':dernier_vue', date('Y-m-d'), PDO::PARAM_STR);
        $requeteSQL->execute();
    } catch (Exception $exception) {
        echo $exception->getMessage();
    }
}
if (isset($_POST['reessaie']) && !empty($_POST['idCarte']) && filter_var($_POST['idCarte'], FILTER_VALIDATE_INT)) {

    try {
        $requeteSQL = "UPDATE  revoit SET niveau=:niveau WHERE id_carte=:id_carte";
        $requeteSQL = $db->prepare($requeteSQL);
        $requeteSQL->bindValue(':niveau', $niveauZero, PDO::PARAM_INT);
        $requeteSQL->bindValue(':id_carte', $_POST['idCarte'], PDO::PARAM_INT);
        $requeteSQL->execute();
    } catch (Exception $exception) {
        echo $exception->getMessage();
    }
}


/**
 * Récupère  niveau de la revision
 *
 * @param [type] $nbrN
 * @param integer $interval
 * @return array les niveaux
 */
function niveauDeRevision($nbrN, int $interval): array
{

    $array = [];
    for ($i = $nbrN; $i > 0; $i--) {
        if (($interval % (2 ** ($i - 1))) == 0) {
            array_push($array, $i);
        }
    }
    return $array;
}

$niveaux = niveauDeRevision($nbrN, $interval);



$cartes = [];

//todo : commenter la boucle
//cette boucle me permet de parcourir le tableau $niveaux et dans chaque niveau on recupère les cartes 
//du niveau concerné de la revision dont la date de vue doit etre differente de la date du jour ou qu'elle n'ai jamais ete vue
//et que leur niveau dans la table revoit ne depasse pas le  nbre de niveau dans revision
foreach ($niveaux as $niveau) {
    # code...
    $req = "SELECT carte.recto, carte.verso, carte.img_recto, carte.img_verso, carte.id_carte,revoit.niveau
            FROM revoit
            INNER JOIN carte ON carte.id_carte = revoit.id_carte 
            INNER JOIN revision ON revision.id_revision = revoit.id_revision 
            WHERE revoit.niveau = :niveau AND revision.id_revision = :id_revision 
            AND (dernier_vue != current_date() OR dernier_vue IS NULL) AND revoit.niveau<=revision.nb_niveau
            ORDER BY niveau DESC";

    ///bind $niveau
    $statement = $db->prepare($req);
    $statement->bindParam("id_revision", $idRevision,);
    $statement->bindParam(":niveau", $niveau,PDO::PARAM_INT);
    $statement->execute();

    $result = $statement->fetchAll();
    $cartes = array_merge($cartes, $result);
}
// var_dump($result);
/////foreach
//  $idCarte = $cartes['id_carte'];
// var_dump($cartes);





$niveau = $nbrN + 1;





?>




<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="app.js" defer></script>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <script src="app.js" defer></script>
    <title>Ma Revision</title>


</head>

<body>

    <?php include "header.php"; ?>
    <div>
        <h1>Ma Révision</h1>
    </div>
    <main class="contenaire">

        <div class="grid">
            <!-- <pre> -->
            <?php
            // print_r($cartes);
            ?>
            <!-- </pre> -->
            <?php foreach ($cartes as $carte) : ?>
                
                <card class="card-wrapper">
                    <div class="card">
                        <p>Niveau<?= $carte['niveau'] ?></p>
                        <div class="double-face active">
                            <div class="face">
                                <article><?= $carte['recto'] ?></article>
                                <img class="img" src="<?= $carte['img_recto'] ?>" alt="">
                            </div>
                            <div class="back">
                                <article><?= $carte['verso'] ?></article>
                                <img class="img" src="<?= $carte['img_verso'] ?>" alt="">
                            </div>
                        </div>

                    </div>
                    <form action="" method="post">
                        <div class="">
                            <input class="input" style="background-color: green ;" type="submit" name="continue" value="Vrai">
                            <input class="input" style="background-color: red " type="submit" name="reessaie" value="Faux">
                            <!--  -->
                            <input type="hidden" name="idCarte" value="<?php echo $carte['id_carte']; ?>">
                        </div>
                    </form>
                </card>
            <?php endforeach; ?>
        </div>

    </main>

</body>

</html>