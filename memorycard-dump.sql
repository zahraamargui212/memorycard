-- MariaDB dump 10.19  Distrib 10.6.7-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: memorycard
-- ------------------------------------------------------
-- Server version	10.6.7-MariaDB-2ubuntu1.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `carte`
--

LOCK TABLES `carte` WRITE;
/*!40000 ALTER TABLE `carte` DISABLE KEYS */;
INSERT INTO `carte` VALUES (12,4,'test1','test2',NULL,NULL,'2022-11-21','2022-11-21'),(13,4,'test image rex','test image verso',NULL,NULL,'2022-11-21','2022-11-21'),(15,4,'test image rex','test image verso',NULL,NULL,'2022-11-21','2022-11-21'),(16,4,'test image rex','test image verso',NULL,NULL,'2022-11-21','2022-11-21'),(17,4,'test image rex','test image verso',NULL,NULL,'2022-11-21','2022-11-21'),(18,7,'test image rex','test image verso',NULL,NULL,'2022-11-21','2022-11-21'),(19,7,'test image rex','test image verso',NULL,NULL,'2022-11-21','2022-11-21'),(20,7,'test image rex','test image verso',NULL,NULL,'2022-11-21','2022-11-21'),(21,13,'test1','test test2',NULL,NULL,'2022-11-21','2022-11-21'),(23,8,'zahra','zahra',NULL,NULL,'2022-11-21','2022-11-21'),(24,7,'test image rex','test image verso',NULL,NULL,'2022-11-21','2022-11-21'),(25,7,'test lio','test lio',NULL,NULL,'2022-11-21','2022-11-21'),(26,7,'test1','test test2','upload/16690348711313442802954991815.png','upload/16690348711313442802954991815.png','2022-11-21','2022-11-21'),(27,15,'','','upload/166903510710767765341395839431.png','upload/166903510710767765341395839431.png','2022-11-21','2022-11-21'),(30,9,'','','upload/166903535511431826981845492715.png','upload/166903535511431826981845492715.png','2022-11-21','2022-11-21');
/*!40000 ALTER TABLE `carte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,'Sport'),(2,'langue'),(4,'musique'),(5,'informatique'),(6,'Musique'),(7,'jeux ');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `revision`
--

LOCK TABLES `revision` WRITE;
/*!40000 ALTER TABLE `revision` DISABLE KEYS */;
INSERT INTO `revision` VALUES (3,3,13,5,10,'2022-11-23'),(21,1,4,4,2,'2022-11-23');
/*!40000 ALTER TABLE `revision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `revoit`
--

LOCK TABLES `revoit` WRITE;
/*!40000 ALTER TABLE `revoit` DISABLE KEYS */;
/*!40000 ALTER TABLE `revoit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `theme`
--

LOCK TABLES `theme` WRITE;
/*!40000 ALTER TABLE `theme` DISABLE KEYS */;
INSERT INTO `theme` VALUES (4,2,1,'Arabe',NULL,1),(7,1,1,'Tennis',NULL,0),(8,1,1,'Rugby',NULL,1),(9,1,1,'Hockey',NULL,1),(13,5,1,'HTML',NULL,1),(15,7,1,'video',NULL,0),(16,7,1,'video',NULL,1),(17,5,3,'HTML',NULL,1),(18,6,3,'test',NULL,0),(19,5,4,'PHP',NULL,1);
/*!40000 ALTER TABLE `theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES 
    (1,'zahra','zahra@gmail.com','$2y$10$sjoI1pfeMWZkQQvH0JtfauBdsoEQjLIwXAuV11tqdCzCDUkOlkcw2'), -- pasword = zahra 
    (2,'chantal','chantal@gmail.com','$2y$10$Wgl7QSRvfHfb9dPo8W8Sc.MpLk6/8SVWfzBUGZmbo6rA3wXYPpFLa'),
    (3,'lisa','lisa@gmail.com','$2y$10$fA3j62ahEGWyIDQ/HpH1YO78OpSpn9ybrQCBbWGLyVtt7u1luMx2y'),
    (4,'lionel','lionel@gmail.com','$2y$10$MeXXeQ8J5tq/A384ZcIKVuSTKSEY6rDo.QDhE8vyDY7xci3.vLr4.'),
    (5,'nicolas','Nico@gmail.com','$2y$10$wJapRGJB1CI10HRFU7SJJ.mqqVR/ysVNnIE0nAnnoGqcKCxeQw9D6'),
    (6,'marwa','marwa@gmail.com','$2y$10$131GmeWEgHhVaGxS2Y/wy.8sZTLJLV4Bzpi4XA9d5AGZPG0eKRuMi'),
    (7,'zahra1','zahra1@gmail.com','$2y$10$CCIdjjgl4kErx4axyjLUbednqpOwwnXWFsNUj40j5HmRgyGzhs6BW'),
    (8,'tintin3','ttttt@hotmail.com','$2y$10$rvIH9GP9D18/0lG1Ps1LiOuQ/utC.b9HChPCTd2ZHt5.XrItxOaRy');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-23 15:49:36
