<?php

// on récupère le fichier session.php et bd.php 
session_start();
require_once 'session.php';
require_once 'bd.php';
include 'header.html';


//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
    header('Location:connexion.php');
}


//verifier l'image a bien recu

function saveImage($image)
{


    $error = 1;


    if ($image['size'] < 3000000) {
        //type
        $informationsImage = pathinfo($image['name']);
        $extensionImage = $informationsImage['extension'];
        $extensionArray = array('jpg', 'png', 'jpeg', 'gif');

        $address = "";

        if (in_array($extensionImage, $extensionArray)) {
            $address = 'upload/' . time() . rand() . rand() . '.' . $extensionImage;

            move_uploaded_file($image['tmp_name'], $address);
            $error = 0;
        }
        return $address;
    }
}



if (isset($_POST['submit'])) {
    // if (isset($_POST['nomTheme']) && isset($_POST['recto']) && isset($_POST['verso']) && isset($_POST['img_recto']) && isset($_POST['img_verso'])) {
    if (isset($_POST['theme']) && isset($_POST['recto']) && isset($_POST['verso'])) {

        $theme = intval($_POST['theme']);
        // print_r($theme);
        $theme = htmlspecialchars($theme);

        $recto = $_POST['recto'];
        $recto = htmlspecialchars($recto);

        $verso = $_POST['verso'];
        $verso = htmlspecialchars($verso);


        if (isset($_FILES)) {

            // on vérifie si des fichiers ont été transmis par le formulaire
            // si oui on save les fichiers
            // pour chaque fichier on récupère son $address
            $images = [];
            // on met à jour la requête pour sauvegarder $address dans la bdd

            foreach ($_FILES as $name => $file) {
                if ($file['size'] > 0) {
                    $images[$name] = saveImage($file);
                }
            }
        }

        $columns = ['id_theme', 'recto', 'verso'];

        $tokens = [':id_theme', ':recto', ':verso'];

        // on vérifie si on a des images
        if (count($images) > 0) {
            // on a besoin d'une string au format ,img_verso
            foreach ($images as $name => $image) {

                // var_dump($name);
                // var_dump($image);
              
                //on recupére image et (push) dans tableau $column et $value  
                array_push($columns, $name);
                array_push($tokens, ":" . $name);
            }
        }

        try {

            $baseSQLRequest = "INSERT INTO carte (" . join(',', $columns) . ") VALUES (" . join(',', $tokens) . ")";
            $requetSql = $db->prepare($baseSQLRequest);
            $requetSql->bindValue(':id_theme', $theme, PDO::PARAM_INT);
            $requetSql->bindValue(':recto', $recto, PDO::PARAM_STR);
            $requetSql->bindValue(':verso', $verso, PDO::PARAM_STR);

            // print_r($images);
            

            if (in_array(':img_recto', $tokens)) {

                $requetSql->bindValue(':img_recto', $images['img_recto'], PDO::PARAM_STR);
            }
            if (in_array(':img_verso', $tokens)) {

                $requetSql->bindValue(':img_verso', $images['img_verso'], PDO::PARAM_STR);
            }


            ////
            $requetSql->execute();
            $revision = $requetSql->fetchAll();
            // header('Location:afficherTheme.php');
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }
}



?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script src="app.js" defer ></script>
    <title>Crée Une Carte</title>
</head>

<body>
<?php include "header.php"; ?>
    <main class="contenaire">
       
        <div class="grid">

            <!--form container-->
            <div class="wrapper">
                <h1>Crée Une Carte</h1>
                <div class="form-container">
                    <form novalidate action="creeUnecarte.php" method="post" enctype="multipart/form-data">
                        <!--flexbox and it's items-->
                        <div class="flex">
                            <div class="flex-item">


                                <div class="field-container">
                                    <label for="theme">Theme:<span class="required">*</span></label>
                                    <!-- <input type="text" name="theme" id="theme" placeholder= "" required /> -->
                                    <?php include('themesOption.php'); ?>
                                    <span class="error-messg"></span>
                                </div>



                                <div class="field-container">
                                    <label for="name">Recto : <span class="required">*</span></label>
                                    <input type="text" name="recto" id="name" placeholder="Ex: foot" required />
                                    <span class="error-messg"></span>

                                    <?php
                                    if (isset($error) && $error == 0) {
                                        echo '<img src="' . $address . '"  id = "image"/>';
                                    } elseif (isset($error) && $error == 1) {
                                        echo 'Votre image ne peut pas être envoyée.Vérifier son extension et sa taille (maximum à 3 mo).';
                                    }

                                    ?>

                                    <p>
                                        <input style="background-color: white;" type="file" required name="img_recto" />
                                    </p>

                                </div>

                                <div class="field-container">
                                    <label for="name">Verso : <span class="required">*</span></label>
                                    <input type="text" name="verso" id="name" placeholder="Ex: foot" required />
                                    <span class="error-messg"></span>

                                    <p>
                                        <input style="background-color: white;" type="file" required name="img_verso" />
                                    </p>
                                </div>


                                <div class="center"><input type="submit" name="submit" value="Valider"> </div>



                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
</body>

</html>