<?php

// on récupère le fichier session.php et bd.php 
require_once 'session.php';
require_once 'bd.php';
session_start();
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
    //  header('Location: ../connexion.php');
}
// echo  $_SESSION['idUser'];
// je vérifie la session
if (isset($_SESSION["msg"])) {
    echo "<div class = 'required center' ><p>{$_SESSION["msg"]}</p></div>";
}
//a continuer

$sql = "SELECT * FROM categorie";
// reparer la requête
$stm = $db->prepare($sql);
// on execute la requête
$stm->execute();

$categories = $stm->fetchAll();




if (isset($_POST['submit'])) {

    print_r($_POST);

    // retrouver l'ID de la catégorie par rapport au nom transmis dans le form
    $selectedCategory = array_values(array_filter($categories, function ($cat) {
        if ($cat["nom"] === $_POST['nomCategorie']) {
            return $cat;
        }
    }));
    // print_r($selectedCategory);



    // check des valeurs reçues en POST
    // print_r($_POST);
    if (!empty($_POST['nomTheme']) && !empty($_POST['nomCategorie'])) {

        $nomTheme = $_POST['nomTheme'];
        $nomTheme = htmlspecialchars($nomTheme);

        $nomCategorie = $_POST['nomCategorie'];
        $nomCategorie = htmlspecialchars($nomCategorie);

        if (isset($_POST['public'])) {
            $public = 0;
        } else {
            $public = 1;
        }

        try {

            $requetSql = "INSERT INTO theme (id_categorie,id_utilisateur,nom,public) VALUES (:id_categorie,:id_utilisateur,:nom,:public) ";
            $requetSql = $db->prepare($requetSql);
            $requetSql->bindValue(':id_categorie', $selectedCategory[0]['id_categorie'], PDO::PARAM_INT);
            $requetSql->bindValue(':id_utilisateur', $_SESSION['idUser'], PDO::PARAM_INT);
            $requetSql->bindValue(':nom', $nomTheme, PDO::PARAM_STR);
            $requetSql->bindValue(':public', $public, PDO::PARAM_INT);

            ////
            $requetSql->execute();
            $revision = $requetSql->fetch();
            header('Location:afficherTheme.php');
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    } else {
        // header('Location:');
    }
}







?>





<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Crée Vos thémes</title>
    <script src="app.js" defer></script>
</head>

<body>
    <?php include "header.php"; ?>

    <main class="contenaire">

        <div class="grid">

            <!--form container-->
            <div class="wrapper">
                <h1>Crée vos Thémes</h1>
                <div class="form-container">
                    <form novalidate action="" method="post" enctype="">
                        <!--flexbox and it's items-->
                        <div class="flex">
                            <div class="flex-item">
                                <div class="field-container">
                                    <label for="name">Categorie : <span class="required">*</span></label>
                                    <input list="ice-cream-flavors" type="text" name="nomCategorie" id="name" placeholder="Ex: Sport" required />
                                    <span class="error-messg"></span>
                                    <datalist id="ice-cream-flavors">

                                        <?php

                                        foreach ($categories as $categorie) { ?>
                                            <option value="<?php echo $categorie['nom'] ?>">

                                            <?php }
                                            ?>
                                    </datalist>

                                </div>

                                <!-- <?php
                                        // echo $selectedCategory[0]['id_categorie'];
                                        ?> -->

                                <div class="field-container">
                                    <label for="name">Théme : <span class="required">*</span></label>
                                    <input type="text" name="nomTheme" id="name" placeholder="Ex: foot" required />

                                    <span class="error-messg"></span>
                                </div>
                                <div>
                                    <input type="hidden" name="id_categorie" value="<?php echo $selectedCategory[0]['id_categorie']; ?>" />
                                </div>

                                <div class="checkbox-wrapper-7">Privée
                                    <input class="tgl tgl-ios" name="public" id="cb2-7" type="checkbox" />
                                    <label class="tgl-btn" for="cb2-7">
                                </div>

                                <div class="center"><input class="submit" type="submit" name="submit" value="Valider"> </div>

                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </main>
</body>

</html>