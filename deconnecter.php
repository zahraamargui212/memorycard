<?php
// on récupère le fichier bd.php
require_once 'bd.php';

// on récupère le fichier bd.php
require_once 'session.php';

// Déconnecte l'utilisateur : 
// si la session n'est pas active
if(session_status() !== PHP_SESSION_ACTIVE) {  
    session_start(); // on la démarre 
}

   
// supprime les variables de sessions
session_unset(); 
// détruit la session 
session_destroy();
// indique au navigateur qu'il faut supprimer le cookie de session
setcookie(session_name(), '', strtotime('-1 day'));

header('Location: connexion.php');
