

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="grid">
        <?php foreach ($cartes as $carte) : ?>
            <div class="card">
                <div class="double-face active">
                    <div class="face">
                        <article><?= $carte['recto'] ?></article>
                        <img class="img" src="<?= $carte['img_recto'] ?>" alt="face">
                    </div>
                    <div class="back">
                        <article><?= $carte['verso'] ?></article>
                        <img class="img" src="<?= $carte['img_verso'] ?>" alt="back">
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</body>

</html>









<!--  -->
<?php

// on récupère le fichier session.php et bd.php 


require_once 'session.php';
require_once 'bd.php';
include 'header.html';
require_once 'functions.php';


//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
    header('Location:connexion.php');
}

//////////////////
$idRevision = $_GET['id_revision'];
/**
 * Récupère  la revision (id_revision)
 * @param int $idRevision
 * @param \PDO $db
 * @return array
 */
function get_revision(\PDO $db, $idRevision)
{
    try {
        $requetSql = "SELECT * FROM revision WHERE id_revision = :id";
        // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
        $requetePreparee = $db->prepare($requetSql);
        $requetePreparee->bindValue(':id', $idRevision, PDO::PARAM_INT);
        // On execute la requête préparée 
        $requetePreparee->execute();
        // On renvoi l'ensemble des résultats de la requête
        return $requetePreparee->fetch();
    } catch (Exception $exception) {
        echo $exception->getMessage();
    }
}
$get_revisions = get_revision($db, $idRevision);
// var_dump($get_revisions);
////////////////////



$started_at = $get_revisions['started_at'];

$origin = new DateTimeImmutable($started_at);
$target = new DateTimeImmutable();
$interval = date_diff($origin, $target)->d;
// echo $interval->format('%R%a days');

$nbrN =  $get_revisions['nb_niveau'];

// echo "<br>$nbrN";


/**
 * Récupère  niveau de la revision
 *
 * @param [type] $nbrN
 * @param integer $interval
 * @return array les niveaux
 */
function niveauDeRevision($nbrN, int $interval): array
{

    $array = [];
    for ($i = $nbrN; $i > 0; $i--) {
        if (($interval % (2 ** ($i - 1))) == 0) {
            array_push($array, $i);
        }
    }
    return $array;
}

$niveaux = niveauDeRevision($nbrN, $interval);

// var_dump($niveaux);

$cartes = [];

foreach ($niveaux as $niveau) {
    # code...
    $req = "SELECT carte.recto, carte.verso, carte.img_recto, carte.img_verso, carte.id_carte,revoit.niveau
            FROM revoit
            INNER JOIN carte ON carte.id_carte = revoit.id_carte 
            INNER JOIN revision ON revision.id_revision = revoit.id_revision 
            WHERE revoit.niveau = $niveau";
    $statement = $db->prepare($req);

    $statement->execute();

    $result = $statement->fetch();

    array_push($cartes, $result);
}

// echo "<pre>";
// print_r($cartes);
// echo "</pre>";
//////////////////////////// ma revision
$idCarte = $carte['id_carte'];
$nbrN =  $get_revisions['nb_niveau'];
// echo $nbrN;
$idRevision = $_GET['id_revision'];





// try {
//     $requetSql = "INSERT INTO revoit (id_carte,id_revision,niveau,dernier_vue) VALUES (:id_carte,:id_revision,:niveau, :dernier_vue)";
//     $requetSql = $db->prepare($requetSql);
//     $requetSql->bindValue(':id_carte', $idCarte, PDO::PARAM_INT);
//     $requetSql->bindValue(':id_revision', $idRevision, PDO::PARAM_INT);
//     $requetSql->bindValue(':dernier_vue', date('Y-m-d') , PDO::PARAM_STR);
//     $requetSql->bindValue(':niveau', $nbrN, PDO::PARAM_INT);
//     $requetSql->execute();
//     $result = $requetSql->fetch();
//     var_dump($result);
// } catch (Exception $exception) {
//     echo $exception->getMessage();
// }

?>






















<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="app.js" defer></script>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Carte</title>
    <style>
        .scene {
            display: inline-block;
            width: 450px;
            height: 260px;
            /*   border: 1px solid #CCC; */
            margin: 40px 0;
            perspective: 600px;
        }

        .card {
            position: relative;
            width: 100%;
            height: 100%;
            cursor: pointer;
            transform-style: preserve-3d;
            transform-origin: center right;
            transition: transform 1s;
        }

        .card.is-flipped {
            transform: translateX(-100%) rotateY(-180deg);
        }

        .card__face {
            position: absolute;
            width: 100%;
            height: 100%;
            line-height: 260px;
            color: white;
            text-align: center;
            font-weight: bold;
            font-size: 40px;
            backface-visibility: hidden;
        }

        .card__face--front {
            background-image: url();
        }

        .card__face--back {
            background: slateblue;
            transform: rotateY(180deg);
        }
    </style>

</head>

<body>
    <main class="main">
        <?php include "nav.php"; ?>
        <div class="wrapperCard">

            <div class="scene scene--card">


                <?php
                foreach ($cartes as $carte) : ?>
                    <?php
                    echo "<pre>";
                    print_r($carte);
                    echo "</pre>";
                    ?>
                    <div class="card">
                        <div class="card__face card__face--front">
                            <article><?= $carte['recto'] ?></article>
                            <img class="img" src="<?= $carte['img_recto'] ?>" alt="face">
                        </div>
                        <div class="card__face card__face--back">
                            <article><?= $carte['verso'] ?></article>
                            <img class="img" src="<?= $carte['img_verso'] ?>" alt="back">
                        </div>

                    </div>
                    <div class="">
                        <input style="background-color: red;" type="submit" name="submit" value="non,on réessaie">
                        <input style="background-color: green ;" type="submit" name="submit" value="oui,on continue!">
                    </div>




                <?php endforeach; ?>
            </div>


        </div>


        <!-- <h1>La RÉPÉTITION ESPACÉE</h1> -->


        <!-- <div class="scene scene--card">
    <div class="card">
        <div class="card__face card__face--front">front</div>
        <div class="card__face card__face--back">back</div>
    </div>
</div> -->


    </main>
    <script src="app.js"></script>
</body>

</html>
<!--  -->