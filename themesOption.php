<?php
require_once "bd.php";


try {
    $requeteSql = "SELECT * FROM theme";
    // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
    $requetePreparee = $db->prepare($requeteSql);
    // On execute la requête préparée 
    $requetePreparee->execute();
    // On renvoi l'ensemble des résultats de la requête
    $themes = $requetePreparee->fetchAll();
} catch (Exception $exception) {
    echo $exception->getMessage();
}


?>

            
                <select  name="theme" id="cat-select">
                    <option value="">Les Thémes</option>
                    <?php foreach ($themes as  $theme) : ?>
                        <option value="<?php echo $theme["id_theme"]; ?>"><?php echo $theme["nom"]; ?></option>
                        <!-- <a href="revision.php?id_theme= value="<?php echo $theme["id_theme"]; ?>> <?php echo $theme["nom"]; ?></a> -->
                    <?php endforeach; ?>
                </select>
           