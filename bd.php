<?php
//etablir connexion PDO
// on va utiliser la class PDO (PHP Data Object) pour se connecter à notre serveur de bdd
// parametre de connexion DSN,username,user password
// $dbh = new PDO('mysql :host ; dbname=test' , $user,$pass);

define('DSN','mysql:host=localhost;dbname=memorycard');
define('USER_NAME','memorycard');
define('USER_PASS','pass_memorycard'); 


try {
    // définition des options de la classe PDO
    $options = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ];

    // $db représente la connexion à la DB
    // en fait c'est une instance de la classe PDO
    $db = new PDO(DSN, USER_NAME, USER_PASS, $options);

    // echo 'Connexion ok à la DB';
} catch (PDOException $error) {
    // on récupère l'erreur au cas où
    echo 'Erreur de connexion à la DB : '.$error->getMessage();
}



?>