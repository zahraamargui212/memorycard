<?php

/**
 * Récupere tous les categories public 
 * @param \PDO $db
 * @return Array  les categories public 
 */
function displayPublicThemes(\PDO $db) : Array
 {
    try {
       
        // $themeSql = "SELECT * FROM theme where public=1 GROUP BY id_categorie; ";
        $themeSql ="SELECT theme.nom AS nomTheme,theme.id_utilisateur,theme.public,categorie.nom AS nomCategorie 
        FROM theme 
        INNER JOIN categorie on theme.id_categorie=categorie.id_categorie 
        WHERE public= 1 
        ORDER BY NomCategorie ;";
        $theme = $db->prepare($themeSql);
        $theme->execute();
        return $theme ->fetchAll();
    } catch (Exception $exception) {
        echo $exception->getMessage();
    }
   
}

/**
 * Undocumented function
 *
 * @param [type] $db
 * @param [type] $idConnectedUser
 * @return void
 */
function displayUserPivateThemes($db, $idConnectedUser)
{   try {
    
    $themeSql = "SELECT theme.nom AS nomTheme,theme.id_utilisateur,theme.public,categorie.nom AS nomCategorie 
    FROM theme 
    INNER JOIN categorie on theme.id_categorie=categorie.id_categorie 
    WHERE public= 0
    AND theme.id_utilisateur = $idConnectedUser
    ORDER BY NomCategorie ;";
    $theme = $db->prepare($themeSql);
    $theme->execute();
    return $theme->fetchAll();
    
}  catch (Exception $exception) {
    echo $exception->getMessage();
}
}

/**
 * Récupere les categories de l'utilisateur connecté 
 * @param \PDO $db
 * @param int $idConnectedUser
 * @return Array  les categories 
 */
function afficherMesCatogorie(\PDO $db, int $idConnectedUser): array
{
    $requetSql = "SELECT * FROM categorie 
    WHERE categorie.id_categorie 
    in (SELECT DISTINCT id_categorie  FROM theme where id_utilisateur = :id)";
    $requetSql = $db->prepare($requetSql);
    $requetSql->bindValue(':id', $idConnectedUser, PDO::PARAM_INT);
    $requetSql->execute();
    return $requetSql->fetchAll();
};

/**
 * Undocumented function
 *
 * @param [type] $db
 * @return void
 */
function mesThemes($db)
{

    $themeSql = "SELECT theme.id_theme,theme.nom AS nomTheme,
    theme.id_categorie,categorie.nom AS nomCategorie,theme.public 
    FROM theme 
    INNER JOIN categorie 
    on categorie.id_categorie = theme.id_categorie 
    WHERE public=1";
    $theme = $db->prepare($themeSql);
    $theme->execute();
    return $theme->fetchAll();
};

/**
 * Récupere les thémes pour une categorie donnée
 * @param \PDO $db
 * @param int $idCategorie
 * @return Array les thémes de la categorie
 */
function themesDeCategorie(\PDO $db, int $idCategorie): array
{
    $requetSql = "SELECT * FROM theme where id_categorie=:id";
    $requetSql = $db->prepare($requetSql);
    $requetSql->bindValue(':id', $idCategorie, PDO::PARAM_INT);
    $requetSql->execute();
    return $requetSql->fetchAll();
}


