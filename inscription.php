<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
// on la démarre une session
session_start();
// on récupère le fichier bd.php
require_once 'bd.php';



// verifier si variable est déclarée et est différente de null
if (isset($_POST['submit'])) {

    // validation email
    $search_html = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

    // sécurisation email
    $search_html = htmlspecialchars($search_html);



    ////////correction///////////////////
    if (! $search_html) {
            echo 'votre email est invalide <a href="index.php">Retour</a> ';
            

    }else{

    /////////////////////correction///////////////////

    // vérifier si l'email existe en BDD (SELECT)
    $sql = "SELECT * FROM utilisateur WHERE email = :email";
    // reparer la requête
    $stm = $db->prepare($sql);

    $data = [
        ':email' => $search_html
    ];
    // on execute la requête
    $stm->execute($data);

    $user = $stm->fetch();
    // si l'email n'existe pas => inscription
    // sinon msg d'erreur
    $pseudo = $_POST['pseudo'];
    $pseudo = htmlspecialchars($_POST['pseudo']);
    /// hashage de mot de password
    $pass = $_POST['passkey'];
    // on verifie que password et Confirm password sont identique
    $search_html = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //Convertit les caractères spéciaux en entités HTML
    $email = htmlspecialchars($_POST['email']);
    //Supprime les espaces  en début et fin de chaîne
    $email = trim($email);
    $pass1 = $_POST["confirm-passkey"];

    if (strlen($pass) < 5) {
        header('Location:index.php');    
    }
    //verifier si mot de pass et confirm-mot de pass est vide
    if (!empty($_POST['passkey'])  && !empty($_POST['confirm-passkey'])) {
        //on compare mot de pass et confirm-mot de pass
        if ($_POST["passkey"] == $_POST["confirm-passkey"]) {
            $hashed_pass = password_hash($pass, PASSWORD_DEFAULT);
            // $search_html = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL); 
            if (!empty($_POST['pseudo'])) {
           
                $data = [
                    ':email' => $search_html,
                    ':password' => $hashed_pass,
                    ':pseudo' => $pseudo,
                ];
                try {
                    // insérer des données ,
                    $sql = "INSERT INTO utilisateur (pseudo,email,password) VALUES (:pseudo,:email,:password)";
                    // reparer la requête
                    $stm = $db->prepare($sql);
                    //execute la requête
                    $stm->execute($data);
                } catch (PDOException $e) {
                    //recuperer l'erreur et le stocker dans un variable pour afficher aprés un message d'erreur 
                    if ($e->errorInfo[1] === 1062) {
                        //  if($e->code === '23000')
                        $_SESSION["msg"] = "Cet email existe déjà";
                        header("Location:index.php");
                    }
                }
                $_SESSION["msg"] = "Utilisateur créé";
                header("Location:connexion.php");
            } else {
                $_SESSION["msg"] = "Le pseudo est obligatoire";

                header("Location:index.php");
            }
        } else {
            $_SESSION["msg"] = "Les mots de passe ne sont pas identiques";
            header('Location:index.php');
        }
    }
}
}

//si l'email existe ou pas
