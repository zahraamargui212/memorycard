<?php
// on récupère le fichier session.php et bd.php 
require_once 'session.php';
require_once 'bd.php';

///////////////////////crée ma revision

///////////////////////


?>



<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta name="description" content=""> -->
    <title>Registreur</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <!--Main wrapper-->
    <div class="wrapper index">
        <h1>Inscription</h1>
        <p id="info">Bienvenue sur MemoryCard, pour en voir plus inscrivez-vous. Sinon,<a href="connexion.php"> connectez-vous</a>.</p>

        <?php
        // je vérifie la session
        if (isset($_SESSION["msg"])) {
            echo "<div class = 'required center' ><p>{$_SESSION["msg"]}</p></div>";
        }
        ?>
        <!--form container-->
        <div class="form-container">
            <form action="inscription.php" method="post" enctype="">
                <!--flexbox and it's items-->
                <div class="flex">
                    <div class="flex-item">
                   <div class="field-container">
                            <label for="pseudo">Pseudo: <span class="required">*</span></label>
                            <input type="text" name="pseudo" id="pseudo" placeholder="Ex: zahra" required />
                            <span class="error-messg"></span>
                        </div>
                        <!--email field-->
                        <div class="field-container">
                            <label for="email">Email: <span class="required">*</span></label>
                            <input type="email" name="email" id="email" placeholder="Ex: zahra@gmail.com" required />
                            <span class="error-messg"></span>
                        </div>
                    </div>
                    <div class="flex-item">
                        <!--password field-->
                        <div class="field-container">
                            <label for="passkey">Mot de passe: <span class="required">*</span></label>
                            <div class="passkey-box">
                                <input type="password" minlength="5" name="passkey" id="passkey" class="passkey" placeholder="******" required />
                                <span class="passkey-icon" data-display-passkey="off"><i class="fas fa-eye"></i> </span>
                            </div>
                            <span class="error-messg"></span>
                     </div>
                    <!--confirm password field-->
                        <div class="field-container">
                            <label for="confirm-passkey">Retaper mot de passe: <span class="required">*</span></label>
                            <div class="passkey-box">
                                <input type="password" name="confirm-passkey" class="passkey" id="confirm-passkey" placeholder="******" required />
                                <span class="passkey-icon" data-display-passkey="off"><i class="fas fa-eye"></i></span>
                            </div>
                            <span class="error-messg"></span>
                        </div>
                   </div>
                </div>
                <!--Submit button & CGU-->
              <div class="center"><input type="submit" name="submit" value="S'inscrire"></div>
            </form>
        </div>
    </div>
    </form>
    </div>

    </div>

</body>

</html>