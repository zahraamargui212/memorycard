<?php
require_once 'bd.php';
require_once 'functions.php';
session_start();
//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
     header('Location: ../connexion.php');
}


$displayPublicThemes = displayPublicThemes($db);



?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script src="app.js" defer></script>
    <title>Afficher themes</title>
</head>

<body>
    <?php include "header.php"; ?>
    <div>
        <h1>Mes Thémes</h1>
    </div>
    <main class="contenaire">


        <div class="grid">

            <?php foreach ($displayPublicThemes as  $theme) : ?>
                <div class="carre"> <a href="mesRevision.php"><?php echo $theme["nomTheme"]; ?></a></div>
            <?php endforeach; ?>

        </div>

    </main>
</body>

</html>