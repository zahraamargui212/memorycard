<?php
require_once 'bd.php';
require_once 'functions.php';
session_start();
//verifier si utilisateur es connecter sinon redirection
if(!isset($_SESSION['idUser'])){
    //redirection vers la page connexion
    //  header('Location: ../connexion.php');
}

// echo "<pre>";
// print_r($displayPublicThemes);
// echo "</pre>";


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Afficher Categorie</title>
</head>

<body>
<main class="contenaire">
        <?php include "nav.php"; ?>
        <a href="ajouterTheme.php">Ajouter un thème</a>
    <div class="grid">
        
            <?php foreach ($displayPublicThemes as  $theme) : ?>
            <div class="carre"> <a href=""><?php echo $theme["nomTheme"]; ?></a></div>
            <?php endforeach; ?>
        
    </div>
    
</main>
</body>

</html>

