<?php
// on récupère le fichier session.php et bd.php 
require_once 'session.php';
require_once 'bd.php';
include 'header.html';
session_start();

if (isset($_POST['submit'])) {
    // je verifier si categorie existe déja dans BDD

    $sql = "SELECT * FROM categorie WHERE nom = :nom";
    // reparer la requête
    $stm = $db->prepare($sql);
    $nomCategorie = $_POST['nom'];
    $data = [
        ':nom' => $nomCategorie
    ];
    // on execute la requête
    $stm->execute($data);

    $categorie = $stm->fetch();


    // si categorier n'existe pas => 
    // sinon msg d'erreur
    try {
        // insérer des données ,
        $sql = "INSERT INTO categorie (nom) VALUES (:nom)";
        // reparer la requête
        $stm = $db->prepare($sql);
        //execute la requête
        $stm->execute($data);
        header("Location:afficherCategorie.php");
    } catch (PDOException $e) {
        //recuperer l'erreur et le stocker dans un variable pour afficher aprés un message d'erreur 
        if ($e->errorInfo[1] === 1062) {
            //  if($e->code === '23000')
            $_SESSION["msg"] = "Cet categorie existe déjà";
            // header("Location:connexion.php");
        }
    }
}




?>






<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script src="app.js" defer></script>
    <title>Crée Un catégorie</title>
</head>

<body>
<?php include "header.php"; ?>

    <!--form container-->
    <main class="contenaire">
        
        <div class="grid">
            <div class="wrapper">
                <h1>Categorie</h1>
                <div class="form-container">
                    <form novalidate action="" method="post" enctype="">
                        <!--flexbox and it's items-->
                        <div class="flex">
                            <div class="flex-item">

                                <div class="field-container">
                                    <label for="name">Nom catégorie : <span class="required">*</span></label>
                                    <input type="text" name="nom" id="name" placeholder="Géographie" required />
                                    <span class="error-messg"></span>
                                </div>
                                
                                <div class="center"><input type="submit" name="submit" value="Valider"></div>

                    </form>
                </div>
            </div>
        </div>
    </main>
</body>

</html>