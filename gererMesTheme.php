<?php

// on récupère le fichier session.php et bd.php 
session_start();
require_once 'session.php';
require_once 'bd.php';
require_once 'functions.php';

//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
    header('Location:connexion.php');
}
////////////////////////

$id = $_SESSION['idUser'];


$displayUserPivateThemes = displayUserPivateThemes($db, $id);
// echo"<pre>";
// print_r($displayUserPivateThemes);
// echo"</pre>";
////////////////////////
$themes = mesThemes($db);
// echo "<pre>";
// print_r($resultats);
// echo "</pre>";
// $afficherMesTheme = [];

// foreach ($resultats as $resultat) {
//     if (!isset($afficherMesTheme[$resultat["nomCategorie"]])) {
//         $afficherMesTheme[$resultat["nomCategorie"]] = [];
//     }
//     array_push($afficherMesTheme[$resultat["nomCategorie"]], $resultat["nomTheme"]);
// }


// $liste_theme = array_keys($afficherMesTheme);

// echo "<pre>";

// print_r($liste_theme);
// echo "</pre>";




$afficherMesCategorie = afficherMesCatogorie($db, $id);
// echo "<pre>";
// print_r($afficherMesCategorie);
// echo "</pre>";





?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script src="app.js" defer></script>
    <title>Gérer Mes Thémes</title>
</head>

<body>
    <?php include "header.php"; ?>
    <div>
        <h1>Gérer Mes Thémes</h1>
    </div>


    <main class="contenaire">

        <div class="grid">

            <!--  -->
            <?php foreach ($afficherMesCategorie as $categorie) : ?>

                <div>
                    <h2> <?php echo $categorie['nom']; ?></h2>
                    <!--  -->



                    <?php
                    $idCategorie = $categorie['id_categorie'];
                    $themes = themesDeCategorie($db, $idCategorie);
                    foreach ($themes as $theme) : ?>
                        <div class="carre">

                            <h2><a href=""><?php echo $theme['nom'] ?></a></h2>
                            <p><?php echo $theme['public'] === 0 ? "Privé" : "Public" ?></p>

                            <br><a href="supprimerTheme.php?id_theme=<?php echo $theme['id_theme'] ?>">Supprimer</a>
                            <br><a href="modifierTheme.php?id_theme=<?php echo $theme['id_theme'] ?>">Modifier</a>
                        </div>

                    <?php endforeach; ?>


                </div>

            <?php endforeach; ?>
        </div>


    </main>



</body>

</html>