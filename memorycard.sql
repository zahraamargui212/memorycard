create database IF NOT EXISTS memorycard;

CREATE USER IF NOT EXISTS 'memorycard'@'localhost' IDENTIFIED BY 'pass_memorycard';

GRANT ALL PRIVILEGES ON memorycard.* TO 'memorycard'@'localhost';

FLUSH PRIVILEGES;

use memorycard;

CREATE TABLE IF NOT EXISTS categorie (
    id_categorie INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom varchar (50) NOT NULL

);

CREATE TABLE IF NOT EXISTS utilisateur (
    id_utilisateur INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    pseudo varchar(50) NOT NULL,
    email varchar(100) NOT NULL,
    password varchar(255) NOT NULL
    
);
CREATE TABLE IF NOT EXISTS theme (
    id_theme INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    id_categorie INT(11) NOT NULL,
    FOREIGN KEY (id_categorie)REFERENCES categorie (id_categorie) ON DELETE CASCADE ON UPDATE CASCADE,
    id_utilisateur INT(11) NOT NULL,
    FOREIGN KEY (id_utilisateur) REFERENCES utilisateur (id_utilisateur) ON DELETE CASCADE ON UPDATE CASCADE,
    nom varchar(50) NOT NULL,
    description Text,
    public boolean NOT NULL
);

CREATE TABLE IF NOT EXISTS carte (
    id_carte INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    id_theme INT (11) NOT NULL,
    FOREIGN KEY (id_theme) REFERENCES theme (id_theme) ON DELETE CASCADE ON UPDATE CASCADE,
    recto varchar(255),
    verso varchar(255),
    img_recto varchar(255),
    img_verso varchar(255),
    date_creation  Date NOT NULL DEFAULT (CURRENT_DATE),
    date_modification  Date NOT NULL DEFAULT (CURRENT_DATE) 
);
CREATE TABLE IF NOT EXISTS revision (
    id_revision INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    id_utilisateur INT (11) NOT NULL,
    FOREIGN KEY (id_utilisateur) REFERENCES utilisateur (id_utilisateur) ON DELETE CASCADE ON UPDATE CASCADE,
    id_theme INT(11) NOT NULL,
    FOREIGN KEY (id_theme) REFERENCES theme (id_theme) ON DELETE CASCADE ON UPDATE CASCADE,
    nb_niveau INT NOT NULL,
    nb_carte INT NOT NULL,
    started_at Date , 
    CONSTRAINT unique_utilisateur_theme UNIQUE (id_utilisateur, id_theme)
);
CREATE TABLE IF NOT EXISTS revoit (
    id_carte INT (11)NOT NULL,
    FOREIGN KEY (id_carte) REFERENCES carte (id_carte) ON DELETE CASCADE ON UPDATE CASCADE,
    id_revision INT(11) NOT NULL,
    FOREIGN KEY (id_revision) REFERENCES revision (id_revision) ON DELETE CASCADE ON UPDATE CASCADE,
    dernier_vue Date,
    niveau INT(11)
);


 UPDATE CARTE SET img_recto ="upload/1669628332890315500651048296.png" WHERE id_carte =26;