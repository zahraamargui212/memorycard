<?php

// on récupère le fichier session.php et bd.php 
require_once 'session.php';
require_once 'bd.php';
// include 'header.html';
require_once 'functions.php';


//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['idUser'])) {
    //redirection vers la page connexion
    header('Location:connexion.php');
}




/**
 * Récupère toutes les cartes 
 * @param \PDO $db
 * @return Array  les cartes 
 */
function getAllCards($db)
{
    try {
        $requetSql = "SELECT * FROM carte";
        // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
        $requetePreparee = $db->prepare($requetSql);
        // On execute la requête préparée 
        $requetePreparee->execute();
        // On renvoi l'ensemble des résultats de la requête
        return $requetePreparee->fetchAll();
    } catch (Exception $exception) {
        echo $exception->getMessage();
    }
}
$cards = getAllCards($db);
// print_r($cards);


// $id =$_GET['id_theme'] ;
// $id = $_GET['id_theme'];
$user = $_SESSION['idUser'];
// echo $user;
/**
 * Récupere toutes les révisions de l'id utilisateur passé en paramètre
 *
 * @param \PDO $db
 * @param int $id
 * @param int $user
 * @return Array les révisions
 */
function getRevisions(\PDO $db, $id, $user)
{
    try {
        $requetSql = "SELECT revision.started_at,revision.nb_niveau,revision.nb_carte,theme.id_utilisateur,revision.id_revision,theme.nom 
                     AS nomTheme,theme.id_theme 
                      FROM revision 
                      inner join theme on theme.id_theme=revision.id_theme
                      WHERE theme.id_utilisateur= :idUser;";
        // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
        $requetePreparee = $db->prepare($requetSql);

        // $requetePreparee->bindValue(':id', $id, PDO::PARAM_INT);
        $requetePreparee->bindValue(':idUser', $user, PDO::PARAM_INT);
        // On execute la requête préparée 
        $requetePreparee->execute();
        // On renvoi l'ensemble des résultats de la requête
        return $requetePreparee->fetchAll();
    } catch (PDOException $exception) {
        echo $exception->getMessage();
    }
}

// echo '<pre>';
// print_r($getRevision);
// echo '</pre>';


function test($db)
{
    try {
        $requetSql = "SELECT * FROM carte 
                    inner join revoit
                    on carte.id_carte = revoit.id_carte 
                    inner join revision 
                    on revision.id_revision = revoit.id_revision;";
        // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
        $requetePreparee = $db->prepare($requetSql);
        // On execute la requête préparée 
        $requetePreparee->execute();
        // On renvoi l'ensemble des résultats de la requête
        return $requetePreparee->fetch();
    } catch (Exception $exception) {
        echo $exception->getMessage();
    }
};





/**
 * devrait renvoyer les niveaux en fonction du nombre de jour 
 *
 * @param [type] $nbrN
 * @param [type] $interval
 * @param [type] $started_at
 * @param [type] $db
 * @return void
 */

$revisions = getRevisions($db, null, $user);
// print_r($revisions);

// foreach($revisions as $revision){
//     echo "<pre>";
//     print_r ($revision);
//     echo "</pre>";

// }


// afficher chaque révisions avec le nom du thème (= faire jointure dans fonction getRevisions), et un lien contenant l'id de la revision
"SELECT revision.started_at,revision.nb_niveau,revision.nb_carte,theme.id_utilisateur,theme.nom AS nomTheme 
FROM revision inner join theme on theme.id_theme=revision.id_theme 
WHERE theme.id_utilisateur= 1;"
// une nouvelle page, ave une fonction qui récupère la révision dont l'id est en paramètre et ... affciher le niveau et les  cartes qui sont dans ce niveau (en utilisant la fonctoin niveau) 

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="app.js" defer></script>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Mes Révisions</title>
</head>

<body>
    <?php include "header.php"; ?>
    <div>
        <h1>Mes Révisions</h1>
    </div>
    <main class="contenaire">

       


        <div class="grid">
            <?php foreach ($revisions as $revision) : ?>
                <div class="carre"> <a href="maRevision.php?id_revision=<?php echo $revision["id_revision"]; ?>&nb_niveau=<?php echo $revision["nb_niveau"]; ?>&id_theme= <?php echo $revision["id_theme"]; ?>">

                        <!-- <a href="maRevision.php"><div class="carre_revision"> -->
                        <h2><?php echo $revision['nomTheme'] ?></h2>
                        <h3>niveau :<?php echo $revision['nb_niveau'] ?></h3>
                        <h4>nombre de carte à reviser :<?php echo $revision['nb_niveau'] ?></h4>
                        <h4>date de debut:<?php echo $revision['started_at'] ?></h4>
                        <!-- récupéré le niveaux et dernier_vue-->


                </div></a>
            <?php endforeach; ?>



        </div>



    </main>
</body>

</html>