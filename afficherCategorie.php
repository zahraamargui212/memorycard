<?php
require_once "bd.php";


session_start();
//verifier si utilisateur es connecter sinon redirection
if (!isset($_SESSION['id_utilisateur'])) {
    //redirection vers la page connexion
    //  header('Location: ../connexion.php');
}


try {
    $requeteSql = "SELECT * FROM categorie";
    // On prépare la requête avec l'objet PDO et on récupère un objet PDOStatement
    $requetePreparee = $db->prepare($requeteSql);
    // On execute la requête préparée 
    $requetePreparee->execute();
    // On renvoi l'ensemble des résultats de la requête
    $categories = $requetePreparee->fetchAll();
} catch (Exception $exception) {
    echo $exception->getMessage();
}


?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script src="app.js" defer></script>
    <title>Afficher Categorie</title>
</head>

<body>
    <?php include "header.php"; ?>
    <div>
        <h1>Mes Catégories</h1>
    </div>

    <main class="contenaire">


        <div class="grid">

            <?php foreach ($categories as  $categorie) : ?>
                <div class="carre"> <a href="gererMesTheme.php?id_categorie=<?php echo $categorie["id_categorie"]; ?>"><?php echo $categorie["nom"]; ?></a><br>

                </div>

            <?php endforeach; ?>
        </div>
    </main>

</body>

</html>