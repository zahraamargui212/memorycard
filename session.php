<?php

// On teste si l'utilisateur est connecté en vérifiant l'existence de la variable de session
if(session_status() !== PHP_SESSION_ACTIVE) { // si la session n'est pas active 
    session_start(); // on la démarre 
}