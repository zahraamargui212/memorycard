

const nav = document.querySelector(".nav")
 
//récupére reférence de l'élément qui a la classe warper-burger
const burger = document.querySelector('.warper-burger');

  // au clic sur l'élément on exécute une fonction callback
  burger.addEventListener("click",()=>{
    // quand on reçoitl'event click si nav contient la classe show on l'enlève, sinon on l'ajoute
  nav.classList.toggle("show");
})